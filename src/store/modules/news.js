
import {
  query_news,
  query_news_by_id,
} from '@/service'

import {
  NEWS_STATIC_HOST as news_static_host
} from '@/service/index.js'


const OPTIONS = {
  UPDATE_NEWS_LIST: 'update_news_list',

  CLEAR_NEWS_LIST: 'clear_news_list',

  UPDATE_NEWS_PAGE:  'update_news_page',

  UPDATE_NEWS_TOTAL: 'update_news_total',

  UPDATE_NEWS_LIMIT: 'update_news_limit',

  UPDATE_NEWS_LOAD_STAUS: 'update_news_load_staus',

  UPDATE_IMG_URL: 'update_img_url',

  START_LOAD: 'start_load',

  END_LOAD: 'end_load',

  UPDATE_NEWS_LIST_INFO: 'update_news_list_info',

  LOAD_NEWS_LIST: 'load_news_list',

  LOAD_FIRST_PAGE_NEWS: 'load_first_page_news',

  LOAD_NEXT_PAGE_NEWS: 'load_next_page_news',

  LOAD_NEWS_BY_ID: 'load_news_by_id',

  GET_NEWS_LIST: 'get_news_list'
}

const initState = {
  news_list: [],
  page: 1,
  limit: 10,
  total: 0,
  more: true,
  on_load: true,
  error_msg: ''
}

const mutations = {
  [OPTIONS.UPDATE_NEWS_LIST] (state, news_list) {
    state.news_list.splice(state.news_list.length, 0, ...news_list);
    
  },
  [OPTIONS.CLEAR_NEWS_LIST] (state) {
    state.page = 1;
    state.total = 0;
    state.news_list.splice(0, state.news_list.length);
  },
  [OPTIONS.UPDATE_IMG_URL] (state) {
    let data = state.news_list;
    for(let news_item of data){
      let val = [];
      try{
        val = JSON.parse( news_item.description )
      }catch(e){
        val = [
           {'insert': news_item.description}
         ]
      }

      if(!val.splice){
        val = [
           {'insert': news_item.description}
         ]
      }

      for(let row of val){
        if(row.insert && row.insert.image){
          let is_url = row.insert.image.indexOf('http://') == 0 || row.insert.image.indexOf('httpw://') == 0
          if(!is_url){
            row.insert.image = `${news_static_host}${row.insert.image}`;
          }
        }
      }

      news_item.description = JSON.stringify(val);
    }

    for(let row of data){
      row.sliders.forEach((i, index) => {
        let base_file_name = i.split('/static/')[1];
        if(base_file_name){
          row.sliders[index] = `${ news_static_host }static/${ base_file_name }`
        }
      });
    }
  },
  [OPTIONS.UPDATE_NEWS_LIMIT] (state, limit) {
    state.limit = limit
  },
  [OPTIONS.UPDATE_NEWS_TOTAL] (state, total) {
    state.total = total;
  },
  [OPTIONS.UPDATE_NEWS_PAGE] (state, page) {
    state.page = page;
  },
  [OPTIONS.UPDATE_NEWS_LOAD_STAUS] (state, status = false) {
    state.on_load = status;
  },
  [OPTIONS.START_LOAD] (state) {
    state.on_load = true;
  },
  [OPTIONS.END_LOAD] (state) {
    state.on_load = false;
  },
  [OPTIONS.UPDATE_NEWS_LIST_INFO] (state, {page, limit, total}) {
    state.page = page;
    state.limit = limit;
    state.total = total;
    if(state.page >= total) {
      state.more = false;
    }else{
      state.more = true;
    }
  }
}

const actions = {
  async [OPTIONS.LOAD_NEWS_LIST] ({state, commit}, params = {tags: []}) {
    commit(OPTIONS.START_LOAD);
    let {is_error, data: base_data} = await query_news({page: state.page, tags: params.tags});
    if (is_error) {
      commit(OPTIONS.END_LOAD);
      return ;
    }
    const {page, limit, total, data} = base_data;
    
    commit(OPTIONS.UPDATE_NEWS_LIST, data);
    commit(OPTIONS.UPDATE_IMG_URL);
    commit(OPTIONS.UPDATE_NEWS_LIST_INFO, {page, limit, total});

    commit(OPTIONS.END_LOAD);
    return data;
  },
  async [OPTIONS.LOAD_NEXT_PAGE_NEWS] ({state, commit, dispatch}, params = {tags: []}) {
    let page  = state.page,
        total = state.total;

    if(!state.more) return;

    if(page >= total) return;

    let next_page = page + 1;

    commit(OPTIONS.UPDATE_NEWS_PAGE, next_page);
    await dispatch(OPTIONS.LOAD_NEWS_LIST, params);

  },
  async [OPTIONS.LOAD_FIRST_PAGE_NEWS] ({state, commit, dispatch}, params = {tags: []}) {
    commit(OPTIONS.CLEAR_NEWS_LIST);
    await dispatch(OPTIONS.LOAD_NEWS_LIST, params);
  },
  async [OPTIONS.LOAD_NEWS_BY_ID] ({state, commit, dispatch}, news_id) {
    let {is_error, data: base_data} = await query_news_by_id({id: news_id});
    commit(OPTIONS.CLEAR_NEWS_LIST);
    commit(OPTIONS.UPDATE_NEWS_LIST, base_data);
    commit(OPTIONS.UPDATE_IMG_URL);
  }
}

const getters = {

}

export default {
    state: initState,
    mutations,
    actions,
    getters,
};