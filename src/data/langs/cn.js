export default {
  'developer': '开发者',
  // lang.data['developer']
  'Github': 'Github',
  // lang.data['Github']
  'Document': '文档',
  // lang.data['Document']
  'One-Click Startup': '快速启动区块链',
  // lang.data['One-Click Startup']
  'Mainnet News': '主网介绍',
  // lang.data['Mainnet News']
  'Latest News': '最新动态',
  // lang.data['Latest News']
  'Introduction': '主网介绍',
  // lang.data['Introduction']
  'Weekly Report': '开发周报',
  // lang.data['Weekly Report']
  'FAQ': '常见问题',
  // lang.data['FAQ']
  'Community Governance': '社区治理',
  // lang.data['Community Governance']
  'Node Information': '节点介绍',
  // lang.data['Node Information']
  'Proposal Publicity': '提案公示',
  // lang.data['Proposal Publicity']
  'Budget System': '预算系统',
  // lang.data['Budget System']
  'Community Forum': '社区论坛',
  // lang.data['Community Forum']
  'EOS Explorer': '区块浏览器',
  // lang.data['EOS Explorer']
  'Download Wallet': '钱包下载',
  // lang.data['Download Wallet']
  'White Paper': '白皮书',
  // lang.data['White Paper']
  'Mainnet': 'EOSC主网',
  'vote_title': '投票',
  // lang.data['Mainnet']
  'Evolve Towards The Decentralized High-Performance Smart Contract Platform': '朝去中心化的高性能智能合约平台的方向持续演进',
  // lang.data['Evolve Towards The Decentralized High-Performance Smart Contract Platform']
  '1-Token-1-Vote': '',
  // lang.data['1-Token-1-Vote']
  'Each token can only be voted for one node': '高性能低延时的共识算法',
  // lang.data['Each token can only be voted for one node']
  'Voting Reward': '',
  // lang.data['Voting Reward']
  'Gain profit by participating in the voting': '友好的智能合约开发',
  // lang.data['Gain profit by participating in the voting']
  'Time Weighted': '',
  // lang.data['Time Weighted']
  'The longer the stake, the higher the voting weight': '基于时间加权系数的选举机制',
  // lang.data['The longer the stake, the higher the voting weight']
  // 'Budget System': '',
  // lang.data['Budget System']
  'Responsible for approving the applications of community budget proposals': '去中心化链上预算系统',
  // lang.data['Responsible for approving the applications of community budget proposals']
  'Apply Budget': '申请预算',
  // lang.data['Apply Budget']
  'Nodes': '主网节点数量',
  // lang.data['Nodes']
  'Accounts': '主网账户总数',
  // lang.data['Accounts']
  'Issued EOSC Budget': '已发放EOSC预算',
  // lang.data['Issued EOSC Budget']
  'Code Submissions': '代码提交次数',
  // lang.data['Code Submissions']
  'Approved FIP': '已通过FIP提案数',
  // lang.data['Approved FIP']
  'Approved Budget Proposal': '已通过预算提案数',
  // lang.data['Approved Budget Proposal']
  'View Details': '查看详情',
  // lang.data['View Details']
  'Latest FIP': '最新FIP提案',
  // lang.data['Latest FIP']
  'Latest Budget Proposal': '最新预算提案',
  // lang.data['Latest Budget Proposal']
  'Community Autonomous Organization': 'EOSC社区自治组织',
  // lang.data['Community Autonomous Organization']
  'EOSC Community is a decentralized autonomous organization, mainly composed of the Secretariat, the Management Committee and the Development Team.': '<b>EOSC社区</b>是一个去中心化的社区自治组织，主要由秘书处、管理委员会、开发组三部分构成。',
  // lang.data['EOSC Community is a decentralized autonomous organization, mainly composed of the Secretariat, the Management Committee and the Development Team.']
  'The Secretariat': '秘书处',
  // lang.data['The Secretariat']
  'The Secretariat of EOSC Mainnet BP Meeting is an open DAC organization composed of the Community and the Development Team. It is mainly responsible for collecting the community proposals, hosting the BP meeting, recording the meeting content and publishing the meeting minutes.': '<strong>EOSC主网</strong>超级节点会议秘书处是由社区和开发团队组成的一个开放的DAC组织，主要职责是收集社区议案、发起和主持讨论会议、记录会议内容、发布会议内容等。',
  // lang.data['The Secretariat of EOSC Mainnet BP Meeting is an open DAC organization composed of the Community and the Development Team. It is mainly responsible for collecting the community proposals, hosting the BP meeting, recording the meeting content and publishing the meeting minutes.']
  'The Management Committee': '管理委员会',
  // lang.data['The Management Committee']
  'EOSFORCE Mainnet Management Committee aims to manage and approve the decentralized budget system proposals, monitor the implementation of the proposer and promote a fairer distribution of the decentralized budget system, referred to as the Management Committee.': '<b>EOSC主网</b>管理委员会旨在管理审批去中心化预算系统提案，监督提案人执行情况，促进去中心化预算系统更加合理公平的分配，简称管理委员会。',
  // lang.data['EOSFORCE Mainnet Management Committee aims to manage and approve the decentralized budget system proposals, monitor the implementation of the proposer and promote a fairer distribution of the decentralized budget system, referred to as the Management Committee.']
  'The Development Team': '开发组',
  // lang.data['The Development Team']
  'EOSFORCE Development Team is a technical team focusing on the bottom of the blockchain with members from world-class technology companies such as Huawei, Tencent, Baidu and Netease. It has a rich experience in blockchain development.': '<b>EOS原力</b>开发团队是资深的区块链3.0技术开发团队， 致力于在实践中探索更加开放的区块链基础设施。<strong>EOS原力团队</strong>在高性能低延时共识算法，区块链虚拟机，隐私计算，跨链，链上治理等领域有着深入的实践和研究，累计为区块链开源社区贡献了100万行代码。',
  // lang.data['EOSFORCE Development Team is a technical team focusing on the bottom of the blockchain with members from world-class technology companies such as Huawei, Tencent, Baidu and Netease. It has a rich experience in blockchain development.']
  'Technical Feature': '技术特性',
  // lang.data['Technical Feature']
  'Decentralized High-Performance Smart Contract Platform': '去中心化的高性能智能合约平台',
  // lang.data['Decentralized High-Performance Smart Contract Platform']
  'More Friendly Smart Contract Development': '更友好的智能合约开发',
  // lang.data['More Friendly Smart Contract Development']
  'Fee-based resource model provides DAPP developers with smooth developing experience. More abundant API make the development of smart contract much easier.': '基于手续费的资源模型为DAPP开发者提供流畅的开发体验，更多样丰富的API让智能合约开发更加方便。',
  // lang.data['Fee-based resource model provides DAPP developers with smooth developing experience. More abundant API make the development of smart contract much easier.']
  'Cross-Chain Service Adaption': '适配跨链服务',
  // lang.data['Cross-Chain Service Adaption']
  'EOSForce team started the development of Codex, implementing the cross-chain mechanism by building Codex.Relay.': '原力团队开启了Codex项目的开发, 通过建立Codex.Relay中继链来实现各个链之间的跨链机制。',
  // lang.data['EOSForce team started the development of Codex, implementing the cross-chain mechanism by building Codex.Relay.']
  'Customizable Blockchain Development Framework': '可定制化的区块链开发框架',
  // lang.data['Customizable Blockchain Development Framework']
  'EOSC team started Codex.io project, which helps developers to quickly start a blockchain based on Codex.io and makes it more friendly to develop blockchain.': '原力团队开启了Codex.io项目, 帮助开发者快速启动一条基于Codex.io的区块链, 大大降低了区块链的开发门槛。',
  // lang.data['EOSC team started Codex.io project, which helps developers to quickly start a blockchain based on Codex.io and makes it more friendly to develop blockchain.']
  'View Development Document': '查看开发文档',
  // lang.data['View Development Document']
  'Blockchain': '区块链',
  // lang.data['Blockchain']
  'View EOSC Open Source Code EOSC': '查看EOSC开源代码',
  // lang.data['View EOSC Open Source Code EOSC']
  'Smart Contract': '智能合约',
  // lang.data['Smart Contract']
  'Smart Contract Development Document': '智能合约开发文档',
  // lang.data['Smart Contract Development Document']
  'Node': '节点',
  // lang.data['Node']
  'Node Operation & Maintenance Deployment Document': '节点运维部署文档',
  // lang.data['Node Operation & Maintenance Deployment Document']
  'Roadmap': '路线图',
  // lang.data['Roadmap']
  '*2019 Q4 Support isomorphic L2 parallel computing chain; Realize the light-verification node': '*2019 Q4 支持同构L2并行计算链, 实现区块轻验证节点',
  // lang.data['*2019 Q4 Support isomorphic L2 parallel computing chain; Realize the light-verification node']
  '*2020 Q2 The L2 computing chain supports heterogeneous chains and the HASH verification of critical chain states': '* 2020 Q2 L2计算链支持异构链, 支持关键链状态的HASH验证',
  // lang.data['*2020 Q2 The L2 computing chain supports heterogeneous chains and the HASH verification of critical chain states']
  '*2020 Q3 Realize the multi-process of the chain nodes; Support universal chain IO functions; Achieve two-way interoperability between the computing chain and the settlement chain': '* 2020 Q3 实现链节点多进程化, 支持通用的链IO功能, 实现计算链与结算链的双向互操作性',
  // lang.data['*2020 Q3 Realize the multi-process of the chain nodes; Support universal chain IO functions; Achieve two-way interoperability between the computing chain and the settlement chain']
  '*2020 Q4 Realize the block composite verification based on other mechanism; Support multiple virtual machine systems; Improve the developer experience': '* 2020 Q4 实现基于其他共识机制的区块复合验证, 支持多种虚拟机系统, 大幅提高开发者体验',
  // lang.data['*2020 Q4 Realize the block composite verification based on other mechanism; Support multiple virtual machine systems; Improve the developer experience']
  '*2021 Q2 Improve the computing chain with basic functions; Add basic services of IPFS storage and inter-chain notification queue via L2 computing chain': '* 2021 Q2 完善基础功能的计算链: 通过L2计算链添加IPFS存储、链间通知队列等基础服务',
  // lang.data['*2021 Q2 Improve the computing chain with basic functions; Add basic services of IPFS storage and inter-chain notification queue via L2 computing chain']
  'Latest News': '最新动态',
  // lang.data['Latest News']
  'Trading Platform': '交易平台',
  // lang.data['Trading Platform']
  'Tool Application': '工具应用',
  // lang.data['Tool Application']
  'EOSC Wallet': 'EOSC钱包',
  // lang.data['EOSC Wallet']
  'Multi-terminal selections to open EOSC mainnet in all directions ': 'EOSC 메인넷 포털을 위한 다중 터미널 지원',
  // lang.data['Multi-terminal selections to open EOSC mainnet in all directions ']
  'PC': 'PC端',
  // lang.data['PC']
  'The EOSC classic wallet with complete functions': 'EOSC经典钱包，功能齐全',
  // lang.data['The EOSC classic wallet with complete functions']
  'Web': '网页端',
  // lang.data['Web']
  'The plugin wallet, the first choice for the DAPP experience': '插件钱包，DAPP体验首选',
  // lang.data['The plugin wallet, the first choice for the DAPP experience']
  'Mobile': '移动端',
  // lang.data['Mobile']
  'The third-party wallet app that can link to EOSC main network at anytime and anywhere': '第三方钱包APP，随时随地链接EOSC主网',
  // lang.data['The third-party wallet app that can link to EOSC main network at anytime and anywhere']
  'Social': '社交',
  'Tools': '工具应用',
  'View More': '查看更多'
}