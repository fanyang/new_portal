import axios from 'axios'
import {
  NEWS_HOST,
  NEWS_STATIC_HOST,
  POST
} from './index'

const BASE_HEADERS = {
  'Acces-Control-Allow-Origin': '*',
  'accept': 'application/json',
  'content-type': 'application/json',
  'mode': 'cors'
}

export const query_news = async (params) => {
  console.log(params)
  console.log('query_news')
  params = params || {};
  params.tag = '社区';
  params.limit = 6;
  const url = NEWS_HOST + 'news/query_news.do';
  let res = await POST(params, url);
  return res;
}

export const query_news_by_id = async (params) => {
  params = params || {};
  const url = NEWS_HOST + 'news/query_news_by_id.do';
  return POST(params, url);
}