import axios from 'axios'


import {
  NEWS_HOST,
  NEWS_STATIC_HOST,
  POST
} from './index'

const BASE_HEADERS = {
  'Acces-Control-Allow-Origin': '*',
  'accept': 'application/json',
  'content-type': 'application/json',
  'mode': 'cors'
}


export const query_plans = async (params) => {
  params = params || {};
  params.tag = '社区提案';
  params.limit = 3;
  const url = NEWS_HOST + 'news/query_news.do';
  let res = await POST(params, url);
  return res;
}

export const query_news_by_tags = async (params = {limit: 2, tag: '', tags: []}) => {
  const url = NEWS_HOST + 'news/query_news.do';
  let res = await POST(params, url);
  return res;
}
