module.exports = {
  apps : [
      {
        name        : "eosforce_portal",
        script      : "server.js",
        interpreter : "node",
        watch       : false,
        env: {
          "NODE_ENV": "development",
        },
        env_production : {
          "NODE_ENV": "production"
        }
      }
  ]
}